var billElAmount = document.getElementById("bill-amount");
var tipElAmount = document.getElementById("tip");
var count = document.getElementById("count");
var sum = document.getElementById("total");

function countSum(bill, tip) {
  
    var billButNumber = Number(bill);
    var tipButNumber = Number(tip)

    var subTotal = billButNumber + (billButNumber * tipButNumber / 100)

  sum.innerHTML = subTotal.toFixed(2);
}

billElAmount.addEventListener("change", countSum);
tipElAmount.addEventListener("change", countSum);

count.addEventListener("click", function () {
  var bill = billElAmount.value;
  var tip = tipElAmount.value;

  countSum(bill, tip);
});

// countSum();
